﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CharState
{
    Idle,
    Walk,
    Run,
    Attack,
    Damage,
    KnockBack,
    Death

}

public class Enemy : MonoBehaviour
{
    Animator anim;
    GameObject player;
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void FixedUpdate()
    {
        if (anim.GetInteger("state") >= (int)CharState.Attack &&
            anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 1)
            return;

        float d = Vector3.Distance(
            transform.position,
            GameManager.GetInstance()
                .player.transform.position);
        if (player)
            if (d < 5f)
                anim.SetInteger("state", (int)CharState.Attack);
            else
                anim.SetInteger("state", (int)CharState.Run);
        else anim.SetInteger("state", (int)CharState.Idle);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerController>())
            player = other.gameObject;
    }
    void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<PlayerController>())
            player = null;
    }

    public void Attack()
    {
        player?.GetComponent<HP>().GetDamage(10);
    }
}
