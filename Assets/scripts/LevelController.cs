﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    [Range(1, 10)] [SerializeField]
    int Sensivity;
    [SerializeField] float maxAng;
    // Update is called once per frame
    void Update()
    {
        Quaternion rot = transform.rotation;

        float v = -Input.GetAxis("Horizontal");
        float h = Input.GetAxis("Vertical");

        rot.x += h * Sensivity / 100;
        rot.x = Mathf.Clamp(rot.x, -maxAng, maxAng);
        rot.z += v * Sensivity / 100;
        rot.z = Mathf.Clamp(rot.z, -maxAng, maxAng);

        transform.rotation = rot;
    }


}
