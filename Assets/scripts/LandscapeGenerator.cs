﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LandscapeGenerator : MonoBehaviour
{
    [SerializeField] GameObject bedrock;
    [SerializeField] GameObject[] prefabs;
    [SerializeField] int size;
    [Range(0.1f, 1f)][SerializeField] float ratio;


    void Start()
    {
        int seed = Random.Range(0, size);
        for (int j = 0; j < size; ++j)
        {
            for (int i = 0; i < size; i++)
            {
                 
                int y =
                    (int)(size * ratio *
                    Mathf.PerlinNoise(
                        ratio * (seed + i) / size, 
                        ratio * (seed + j) / size
                        )
                    );

                int index =
                    (int)(y *
                    prefabs.Length
                    / (size * ratio));

                GameObject.Instantiate(
                    prefabs[index],
                    new Vector3(j, y, i),
                    transform.rotation,
                    transform
                    );
            }
        }
    }
}
