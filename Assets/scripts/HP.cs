﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HP : MonoBehaviour
{
    [Range(1, 100)][SerializeField] int MaxHealth;
    int health;
    [SerializeField] GameObject destroyed;
    Animator anim;

    [Header("UI")]
    [SerializeField] Text hpText;
    [SerializeField] Image hpBar;
    void Start()
    {
        anim = GetComponent<Animator>();
        health = MaxHealth;
        if (hpText) hpText.text = health.ToString();
    }
    public void GetDamage(int dmg)
    {
        health -= dmg;
        health = Mathf.Clamp(health, 0, health);

        if (anim) anim.SetInteger("state", (int)CharState.Damage);

        if (hpText) hpText.text = health.ToString();
        if (hpBar) hpBar.fillAmount = (1f*health) / MaxHealth;
        if (health <= 0)
        {
            if (destroyed)
                GameObject.Instantiate(
                    destroyed,
                    transform.position,
                    transform.rotation
            );
            if (anim)
            {
                anim.SetInteger("state", (int)CharState.Death);

                Destroy(GetComponent<Rigidbody>());
                foreach (Collider c 
                    in GetComponents<Collider>())
                {
                    Destroy(c);
                }
            }
            else
                Destroy(gameObject);

            Destroy(GetComponent<Enemy>());
        }
    }
}
