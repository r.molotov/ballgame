﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallController : MonoBehaviour
{
    bool isDone = false;
    [SerializeField] Text goText;

    void OnCollisionEnter(Collision collision)
    {
        if (!isDone && collision.collider
            .gameObject.tag == "Terrain")
        {
            goText.enabled = true;
            isDone = true;
            //Destroy(gameObject);
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (!isDone && GameManager.GetInstance().score > 0 && other.tag == "Finish")
        {
            print("you win");
            isDone = true;
        }
        if (!isDone && other.tag == "Coin")
        {
            Destroy(other);
            other.gameObject
                .GetComponent<Animator>()
                .Play("collect");

            GameManager.GetInstance().score++;

            float k = other.gameObject
                .GetComponent<Animator>()
                .GetCurrentAnimatorStateInfo(0)
                .length;
            Destroy(other.gameObject, k);
        }
    }
}
