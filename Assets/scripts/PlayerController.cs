﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [Header("Crosshair")]
    [SerializeField] Sprite chIdle;
    [SerializeField] Sprite chOver;
    [SerializeField] Image crosshair;

    [SerializeField] Text hint;
    [SerializeField] Image weaponIcon;
    [SerializeField] int baseDamage;

    int weaponDamage;
    public int[] matsCount
        { get; private set; } 
        = new int[5];
    public int[] blocksCount
        { get; private set; } 
        = new int[5];

    Sprite defaultIcon;
    Animator anim;
    Camera cam;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponentInChildren<Animator>();
        cam = GetComponentInChildren<Camera>();
        defaultIcon = weaponIcon.sprite;

        GameManager.GetInstance().player = this;
    }
        
    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        Physics.Raycast(
            cam.transform.position,
            cam.transform.forward,
            out hit,
            2.0f
            );
        InteractiveItem itemToInteract;
        HP enemy;
        if (hit.collider)
        {
            itemToInteract = 
                hit.collider
                .GetComponent<InteractiveItem>();
            enemy = hit.collider.GetComponent<HP>();
        }
        else
        {
            itemToInteract = null;
            enemy = null;
        }

        if (itemToInteract || enemy)
        {
            crosshair.sprite = chOver;
            if (itemToInteract)
            {
                hint.text = "Press <E> to use " +
                   itemToInteract.Name;
            } else {
                hint.text = "";
            }
        } else {           
            crosshair.sprite = chIdle;
        }

        if (Input.GetMouseButtonDown(0))
        {
            anim.SetTrigger("attack");
            if (enemy && anim.transform.childCount > 0)
                enemy.GetDamage(baseDamage + weaponDamage);
        }
        if (Input.GetKeyDown(KeyCode.G))
        {
            DropWeapon();
        }
        if (itemToInteract && Input.GetKeyDown(KeyCode.E))
        {
            itemToInteract.Use();
            
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            GameManager
                .GetInstance()
                .ShowInventory();
        }

    }

    void OnTriggerEnter(Collider other)
    {
        ID data = other.GetComponent<ID>();
        if (data)
        {
            ChangeMats(data.id, 1);
            Destroy(other.gameObject);
        }
    }

    public void ChangeMats(int id, int value)
    {
        matsCount[id] += value;
        GameManager.GetInstance().qp.GetChild(id)
            .GetComponentInChildren<Text>()
            .text = matsCount[id].ToString();
    }
    public void ChangeBlocks(int id, int value)
    {
        blocksCount[id] += value;
        GameManager.GetInstance().qp.GetChild(matsCount.Length + 1 + id)
            .GetComponentInChildren<Text>()
            .text = blocksCount[id].ToString();
    }

    void DropWeapon()
    {
        if (anim.transform.childCount == 0) return;
        anim.transform.GetChild(0)
            .GetComponent<Rigidbody>().isKinematic = false;
        anim.transform.GetChild(0).SetParent(null);
        weaponIcon.sprite = defaultIcon;
        weaponDamage = 0;
    }
    public void ChangeWeapon(Weapon w)
    {
        DropWeapon();

        weaponIcon.sprite = w.Icon;
        w.transform.SetParent(anim.transform);
        w.transform.localPosition = Vector3.zero;
        w.transform.localRotation = Quaternion.identity;

        weaponDamage = w.Damage;
        w.GetComponent<Rigidbody>().isKinematic = true;
    }
}
