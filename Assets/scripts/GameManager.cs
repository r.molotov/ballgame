﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    #region Singleton
    public static GameManager GetInstance()
    {
        return _instance;
    }
    private static GameManager _instance;

    void Awake()
    {
        if (!_instance) _instance = this;
        DontDestroyOnLoad(this);
    }
    #endregion Singleton

    public int score;
    [Header("Inventory")]
    [SerializeField] RectTransform content;
    [SerializeField] RectTransform craftTable;
    public RectTransform ip;
    public RectTransform qp;
    [SerializeField] GameObject slotPrefab;

    [HideInInspector] public PlayerController player;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Exit();
    }

    public void StartGame()
    {
        LoadGame(1);
    }
    public void LoadGame(int index)
    {
        SceneManager.LoadScene(index);
    }

    public void Exit()
    {
        // some code
        Application.Quit();
    }

    public void ShowInventory()
    {
        bool current = ip.gameObject.activeInHierarchy;

        ip.gameObject.SetActive(!current);

        player
            .GetComponent<FirstPersonController>()
            .enabled = current;

        Cursor.visible = !current;
        if (!current) Cursor.lockState = 
            CursorLockMode.None;

        Time.timeScale = current ? 1 : 0;

        if (current) ClearInvenory();
        else InitInventory();
    }

    void InitInventory()
    {
        for (int i = 0; i < player.matsCount.Length; i++)
        {
            for (int j = 0; j < player.matsCount[i]; j++)
            {
                GameObject s = GameObject.Instantiate(
                slotPrefab,
                content
                );
                s.transform
                    .GetChild(0)
                    .GetComponent<Image>()
                    .sprite =

                    qp.GetChild(i).GetChild(0)
                    .GetComponent<Image>()
                    .sprite;

                s.transform
                    .GetChild(0)
                    .GetComponent<ID>().id = i;
            }
        }

        foreach (Transform t in ip)
        {
            if (t.GetComponent<ID>())
            Destroy(t.gameObject);
        }
    }
    void ClearInvenory()
    {
        foreach (Transform slot in content)
        {
            Destroy(slot.gameObject);
        }
        ClearCraftTable();
    }
    public void Craft()
    {
        if (craftTable
            .GetChild(0).childCount == 0)
            return;
        int id = craftTable
            .GetChild(0).GetChild(0)
            .GetComponent<ID>().id;

        foreach (Transform s in craftTable)
        {
            if (s.childCount == 0 ||
                s.GetChild(0)
                .GetComponent<ID>()
                .id != id) return;
        }
        player.ChangeMats(id, -4);
        player.ChangeBlocks(id, 1);
        ClearCraftTable();
        print("crafted");
    }

    void ClearCraftTable()
    {
        foreach (Transform s in craftTable)
        {
            if (s.childCount > 0)
                Destroy(s.GetChild(0).gameObject);
        }
    }
}
