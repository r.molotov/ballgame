﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : InteractiveItem
{
    public Sprite Icon;
    public int Damage;

    public override void Use()
    {
        base.Use();
        GameManager.GetInstance()
            .player.ChangeWeapon(this);
    }
}
